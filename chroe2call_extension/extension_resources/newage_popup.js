popup = {};

popup.countrycode = 'US';
popup.selectedconversation = null;
popup.new_message = null;
popup.contact = null;

devicesmap = new Array();
activedevices = new Array();

popup.request_success = function(data) {
	console.log(data);

	$("#div_loading").hide();
	$("#contnentarea").show();

	if(!util.assertLoggedIn(data)) {
		return;
	}

	// Log in was successful.
	$("#login_required").hide();
	$("#login_userhomeview").show();

	if (data.devices && data.devices.length == 0) {
		$("#deviceslist").hide();
		$("actionbar").hide();
		$("recentEventsHeading").hide();
		$("#no_devices_msg").show();
		FluidNav.init('home');
		$('#android').on("click", function(e) {
			e.preventDefault();
			chrome.tabs.create({"url": 'https://play.google.com/store/apps/details?id=com.lahiru.xmes'});
		});
	} else {
		$("#no_devices_msg").hide();
		$("actionbar").show();
		$("recentEventsHeading").show();
		$("[href='#home']").text('Dashboard');
		chrome.extension.sendMessage({status: "logginin"}, function() {});

		$('#showDeviceList').click(function(e) {
			$( "#deviceslist" ).dialog({
				modal: true,
				width: 390
			});
		});

		version = 0;
		$.each(data.devices, function(i, device) {
			devicesmap[device.extensionDeviceId] = device;
			if (device.version > version) {
				version = device.version;
			}

			if (device.authenticated) {
				activedevices.push(device);
				var activateButtonLabel = "Deactivate";
				var activeStatus = "Active";
				//TODO Find a better way to set the country.
				popup.countrycode = device.countryCode;
				$('#phonelist_sms').append($("<option></option>")
				         .attr("value",device.extensionDeviceId)
				         .text(device.deviceName));
				$('#phonelist_calls').append($("<option></option>")
				         .attr("value",device.extensionDeviceId)
				         .text(device.deviceName));
			} else {
				var activateButtonLabel = "Activate";
				var activeStatus = "Not Active";
			}

			if (version < environment.CURRENT_VERSION) {
				shownotification("Please update Deskphone android app to the latest version.", false, 10000);
			}

			// Add device to the table.
			namecolumn = '<td class="device_namecolum"><p>'+ device.deviceName +'</p></td>';
			numcolumn = '<td class="device_numcolum"><p>'+ formatLocal(popup.countrycode, device.phoneNumber) +'</p></td>';
			activeStatus = '<td class="device_numcolum"><p>' + activeStatus + '</p></td>';
			deviceelement = '<tr>' + namecolumn +numcolumn + activeStatus +'</tr>';
			$('#devicelisttable').append(deviceelement);
		});

		// Handle contact details
		// If this is the first time (contactsetversion = -1) then we do it here. Otherwise ask
		// the background page to perform the contact sync.
		if (version >= environment.CONTACT_SUPPORTED_SINCE && data.contactSetVersion > getContactSetVersion()) {
			try {
				if (getContactSetVersion() == -1) {
					shownotification("Updating phone book. Please close the extension and open it again for the changes to take effect.", false, 3000);
					syncContacts(data.contactSetVersion, popup.countrycode);
				} else {
					// Ask background page to sync contacts.
					chrome.extension.sendMessage({
						status : "update_ontacts",
						version : data.contactSetVersion,
						country : popup.countrycode
					}, function() {
					});
				}
			} catch (e) {
				console.log(e);
			}
			
		} else {
			console.log('Contact syncing not supported.');
		}

		// wait until contact sync is done.
		console.log('Contacts sync done');

		if (activedevices && activedevices.length > 0) {
			// Enable other menus
			var callsMenu = '<li><a href="#calls">Calls</a></li>';
			$('#navigationList').prepend(callsMenu);

			var smsMenu = '<li><a href="#sms">SMS</a></li>';
			$('#navigationList').prepend(smsMenu);

			var startWithSms = populateRecentEvents();
			setupMissedCalls();
			//cleanupRecentEvents();

			setupSocialLinks();

			reload_popup_sms();
			showConversation('c_start_new');
			$('#sendcallrequest').addClass("selectable_element_selected");

			if (version >= environment.CONTACT_SUPPORTED_SINCE) {
				try {
				var contacts = getContacts();
				$("#phonenumber").autocomplete({
					source : contacts,
					select : function(event, ui) {
						popup.contact = ui.item;
						$("#send_message").removeAttr("disabled");
					}
				}).data("ui-autocomplete")._renderItem = function(ul, item) {
					return $('<li zoom="0.5">').append(
							"<a><b>" + item.label + "<br></b><p>" + item.desc
									+ "</p></a>").appendTo(ul);
				};
				} catch (e) {
					console.log(e);
				}
			}
		}

		if (startWithSms) {
			FluidNav.init('sms');
		} else {
			FluidNav.init('home');
		}

		console.log(getContactSetVersion());


// if (getContactSetVersion() == 0) {
////			shownotification(
////					"Please update your Deskphone android application to the latest version",
////					false, 10000);
//		}
	}
};

popup.request_failed = function(jqXHR, textStatus, e) {
	$("#div_loading").hide();
	$("#div_error").show();
	console.log("Server connection error. " + e);
};

// Starting point
$(document).ready(function() {

	//Request user configuration (E.g. Phone no, contacts)
	$.get(environment.userconfig_url)
		.success(popup.request_success)
		.error(popup.request_failed);
	$('#send_message').click(send_message_);

	$("label").inFieldLabels();

	setupPlaceCallWindow();
 });

populateRecentEvents = function() {
	var msgs = getRecentMessages();
	var calls = getRecentMissedCalls();
	var startWithSms = false;

	msgi = 0;
	callsi = 0;
	index = 0;

	if (0 == msgs.length && 0 == calls.length) {
		span = $($(document.createElement('span'))).append('<I>No new events </I>');
		$('#recentEventList').append(span);
		startWithSms = true;
	}
	
	while (msgi < msgs.length || callsi < calls.length) {
		try {
		isMsg = false;
		if (msgi < msgs.length && callsi < calls.length) {
			// select which one to print next based on the time.
			if (msgs[msgi].time > calls[callsi].time) {
				x = msgs[msgi++];
				isMsg = true;
			} else {
				x = calls[callsi++];
				isMsg = false;
			}
		} else if (msgi < msgs.length) {
			x = msgs[msgi++];
			isMsg = true;
		} else {
			x = calls[callsi++];
			isMsg = false;
		}

		styleName = isMsg ? 'smsheader' : 'missedcallheader';
		action = isMsg 
				?'View / Reply' 
				:'Callback';
		type = isMsg ? 'SMS' : 'Missed Call';
		var diaplay = x.contactName != '?' ? x.contactName : formatLocal(popup.countrycode, fix_phonenumber_spaces(x.no));

		li = $(document.createElement('li')).attr("class", "new").attr("style", "margin-top: 10px;");
		span = $($(document.createElement('span')).append('<b class="' + styleName +'">' + type +'</b> ')
				.append(diaplay + ' at ' + formatTime(x.time) + ' '));
		a = $(document.createElement('a')).attr("href", "#")
			.attr("ismsg", isMsg ? 1 : 0).attr("no", x.no).attr("conversationid", "C_"+ x.phoneId + "_" + x.no.substring(1))
			.html(action);
		span.append(a);
		li.append(span);

		a[0].onclick = function(e) {
			console.log('Hello');
			ismsg = $(e.currentTarget).attr('ismsg');
			num = $(e.currentTarget).attr('no');

			if (ismsg == 1) {
				FluidNav.init('sms');
				var conversation = $(e.currentTarget).attr('conversationid');
				showConversation(conversation);
			} else if (ismsg == 0) {
				var no = formatE164(popup.countrycode, num);
				placeCall_(null, no, 'Returning call');
			}
		};

		$('#recentEventList').append(li);
		}catch(e) {
			console.log(e);
		}
	}

	// TODO (Modify this to have unread conversation list save 
	// to a temp buffer and hightlight whatever that is still unread).
	cleanupRecentEvents();
	return startWithSms;
};

handleQuickLink = function(isCall, param) {
	console.log(isCall + ' - ' + param);
};

setupPlaceCallWindow = function() {
	selectCallOption('sendcallrequest');
	$('#sendcallrequest').click(function() {
		selectCallOption('sendcallrequest');
	});
	$('#missedcalls').click(function() {
		selectCallOption('missedcalls');
	});
	$("#placeCall").attr('disabled','disabled');

	$("#phonenumber_calls").bind("change",onTextChangeCalls);
	$('#phonenumber_calls').keyup(onTextChangeCalls);

	$('#placeCall').click(placeCall_);
};

setupMissedCalls = function() {
	// load recent missed calls.
	var recentCallsMap = new Array();
	var recentMissedCalls = getRecentMissedCalls();
	for (var j =0; j < recentMissedCalls.length; j++) {
		var x = recentMissedCalls[j];
		recentCallsMap[x.no + '_' + x.time] = x;
	}

	var missedCalls = getMissedCalls();
	for (var i=0; i < missedCalls.length; i++) {
		var call = missedCalls[i];

		time =  new Date(call.time);

		call_id = call.no + '_' + call.time;

		if (recentCallsMap[call_id]) {
			var clazz = 'new';
			var itemType = 'New';
		} else {
			var clazz = 'fix';
			var itemType = 'Previous';
		}

		var no = call.no;
		if (no && no.charAt(0) == ' ' ) {
			no = '+' + no.substr(1);
		}
		var item = '<li class="' + clazz + '"><input type="radio" name="missedcallgrp"' + 
			' class="missedcallcheckbox" value="' + no + '"><span style="margin-left: 2px;">' + 
			'<b> ' + itemType + '</b></span> ' + formatLocal(popup.countrycode, no) + ' at '+ formatTime(time) +'</li>';
		$('#missedcallslist').append(item);
	}

	if (missedCalls.length > 0) {
		$('#missedcalloptions').show();
	} else {
		$('#missedcallslist').append('No missed calls.');
	}

	$('input[name= missedcallgrp]:radio').click(function() {
		$('#btnCallBack').show();
	});

	$('#btnClearMissedCalls').click(function() {
		cleanupAllMissedCalls();
		$('#missedcalloptions').hide();
		$('#missedcallslist').text('');
		$('#missedcallslist');
		setupMissedCalls();
	});

	$('#btnCallBack').click(function() {
		var no = formatE164(popup.countrycode, $('input[name= missedcallgrp]:radio').val());
		placeCall_(null, no, 'Returning call');
	});
};

placeCall_ = function(e, number, note) {

	if(!note) {
		note = $("#note").val();
	}

	if (!number) {
		number =  $('#phonenumber_calls').val();
	}

	if (!note || note=="") {note = "Remind me to call " + number;}
	var url = environment.request_call_url + 'no=' + encodeURIComponent(number) +
		'&device=' + $("#phonelist_calls").val() + '&note=' + encodeURIComponent(note) +
		'&time=' +  new Date().getTime();
	$.get(url)
		.success(function(data) {
				if ("OK" == data) {
					shownotification('Call request sent to phone.' , false);
				} else {
					shownotification('Could not send call request: ' + data , true);
				}
			})
		.error(function() {
			shownotification('Could not send call request: Internal server error.' , true);
		});
};

onTextChangeCalls = function(){
	$('#fieldstatus_calls').show();
	var formatted = formatLocal(popup.countrycode, $('#phonenumber_calls').val());
	$('#phonenumber_calls').val(formatted);
	var phoneUtil = i18n.phonenumbers.PhoneNumberUtil.getInstance();
	var n = null;
	try {
		if ($('#phonenumber_calls').val().trim().length != 0) {
			n = phoneUtil.parseAndKeepRawInput(formatted, popup.countrycode);
		}
	} catch (e) {
		console.log('Could not parse "' + $('#phonenumber_calls').val() + '"');
	}

	if (n && phoneUtil.isValidNumber(n, popup.countrycode)) {
		$("#placeCall").removeAttr("disabled");
		$("#fieldstatus_calls").attr("src","img/validation_ok.png");
	} else {
		$("#placeCall").attr("disabled", "disabled");
		$("#fieldstatus_calls").attr("src","img/validation_notok.png");
	}
};

selectCallOption = function(id) {
	var enabling = null;
	var disabling = null;
	if (id == 'sendcallrequest') {
		disabling = $("#missedcalls")
		enabling = $('#sendcallrequest');
		$('#newcallcontainer').show();
		$('#missedcallscontainer').hide();
	} else {
		$('#newcallcontainer').hide();
		$('#missedcallscontainer').show();
		enabling  = $("#missedcalls")
		disabling = $('#sendcallrequest');
	}

	disabling.removeClass("selectable_element_selected");
	enabling.addClass("selectable_element_selected");
};

reload_popup_sms = function() {
	var conversationList = localStorage.conversations;
	$( "#selectable").html('');
	$( "#selectable")
		.append('<li id="c_start_new" class="selectable_element">New Conversation</li>');
	$("#c_start_new").click(function (e) {
		add_selectionstyle('c_start_new');
	});
	
	if (conversationList) {
		conversationList = JSON.parse(conversationList);

		for (var conv in conversationList.reverse()) {
	
			var c = conversationList[conv];
		
			// If this is not an array we are using old data. Re-set all of them.
			try {
				var messages = JSON.parse(localStorage[c]);
			} catch (e) {
				messages = null;
			}
			
			if (!$.isArray(messages)) {
				localStorage.removeItem(c) ;
				localStorage.removeItem('conversations');
				break;
			}

			// Load first conversation of the list.
			c = messages[0];
			var from = c.no;
			if (c.contactName != '?') {
				from = c.contactName;
			} else {
				from = formatLocal(popup.countrycode, from);
				// If the number is not valid, then we display whatever the content it shows.
				// But needs to disable sending replies to this message.
				if (from.length == 0) {
					from = c.no;
				}
			}
			if (c) {
				$( "#selectable")
					.append('<li id="'+ conversationList[conv] +'" class="selectable_element">' + from + '</li>');
				$("#" + conversationList[conv]).click(conversationList[conv], function (e) {
					add_selectionstyle(e.data);
				});
			}
		}
	}
	$( "#selectable").selectable();
	$( "#selectable").bind( "selectableselected", show_conversations_);

//	if (popup.selectedconversation) {
//		$('#selectable')
//		.find('li')
//		  .removeClass('ui-selected')
//		  .end();
//		$("#"+popup.selectedconversation)
//		  .addClass('ui-selected');
//		refresh_conversation_view_(popup.selectedconversation);
//	}
};

add_selectionstyle = function(to) {
	$('#selectable li').removeClass("selectable_element_selected");
	$('#'+to).addClass("selectable_element_selected");
};

show_conversations_ = function(event, ui) {
	var conversation = ui.selected.id;
	showConversation(conversation);
};

showConversation = function(conversation) {
	$("#message_window").show();
	$('#txtNewMessage').keyup(count_characters_);
	popup.selectedconversation = conversation;

	popup.contact = null;
	if (conversation == 'c_start_new') {
		// Show new conversation options. Resize conversation view. and disable send button.
		$('#newconvoptions').fadeIn(500);
		$('#message_view').removeClass().addClass('conversationview');
		$("#send_message").attr('disabled','disabled');
		$('#message_view').text('');
		$('#phonenumber').val('');

		$("#phonenumber").bind("change",onponenumberchange_sms);
		$('#phonenumber').keyup(onponenumberchange_sms);
	} else {
		$('#newconvoptions').hide();
		$('#message_view').removeClass().addClass('conversationview_exisitingmsg');
		$("#send_message").removeAttr('disabled');
		refresh_conversation_view_(conversation);

		//TODO Remove this ugly piece of code.
		var phoneNumber = JSON.parse(localStorage[conversation])[0].no;
		// If not a valid number: Disable sending replies to this number
		if (!isAPhoneNumber(phoneNumber)) {
			$("#send_message").attr('disabled','disabled');
			$("#send_message").html('Cannot reply. Invalid Number');
			$("#send_message").removeClass().addClass('button pink');
		} else {
			$("#send_message").removeAttr('disabled');
			$("#send_message").html('Send &#x2192;');
			$("#send_message").removeClass().addClass('button white');
		}
	}
	add_selectionstyle(conversation);
};

onponenumberchange_sms = function(){
	popup.contact = null;
	// Start formatting only if the content length is more than 3, and looks like its
	// going to be a phone number.
	current = $('#phonenumber').val();
	if (!current || current.trim().length < 3 || !isAPhoneNumber(current)) {
		console.log('Not valid lu: ' + current); 
		return;
	}

	var formatted = formatLocal(popup.countrycode, $('#phonenumber').val());
	$('#phonenumber').val(formatted);
	var phoneUtil = i18n.phonenumbers.PhoneNumberUtil.getInstance();
	var n = null;
	try {
		if ($('#phonenumber').val().trim().length != 0) {
			n = phoneUtil.parseAndKeepRawInput(formatted, popup.countrycode);
		}
	} catch (e) {
		console.log('Could not parse "' + $('#phonenumber').val() + '"');
	}
	if (isAPhoneNumber(current)) {
		$("#send_message").removeAttr("disabled");
	} else {
		$("#send_message").attr("disabled", "disabled");
	}
};

count_characters_ = function() {
	var charCount = $("#txtNewMessage").val().length;
	var msgCount = Math.round(charCount / 160) + 1;

	$("#character_count").text((charCount%160) + ' / 160 Message '+ msgCount);
};

send_message_ = function() {
	var conversation = null;
	if ('c_start_new' != popup.selectedconversation) {
		conversation = JSON.parse(localStorage[popup.selectedconversation]);
	}
	
	var msgContents = $("#txtNewMessage").val();
	if (!msgContents || msgContents.trim().length == 0) {
		shownotification('Cannot send an empty message.' , true);
		return;
	}
	
	var newMessage = new Object();
	newMessage.time = new Date().getTime();
	newMessage.type = "OUTGOING_SMS";
	newMessage.messageContents = $("#txtNewMessage").val();
	newMessage.sent_by_me = true;

	if (conversation) {
		newMessage.phoneId = conversation[0].phoneId;
		newMessage.no = conversation[0].no;
		newMessage.contactName = conversation[0].contactName;
	} else {
		current = $('#phonenumber').val();
		number = current;
		var countrycode = countryForE164Number(formatE164(popup.countrycode, current));
		if (!countrycode || countrycode.trim().length == 0) {
			console.log('This is not a valid country code. Send this as a no-format number.' + current);
			// Leave it as it is.
		} else {
			number = formatE164(popup.countrycode, $('#phonenumber').val());
		}

		newMessage.phoneId = $("#phonelist_sms").val();
		newMessage.no = number;
		newMessage.contactName = popup.contact ? popup.contact.name :'?';
	}

	popup.new_message = newMessage;
	console.log(popup.new_message);

	$("#send_message").attr('disabled','disabled');

	$.get(environment.send_sms + "?m=" + encodeURIComponent(JSON.stringify(newMessage)))
		.success(function(data){
			if ("OK" == data) {
				//TODO: Handle error logic.
			} else {
				$("#txtNewMessage").val(popup.new_message.messageContents);
				shownotification('Message sending failed. Please try again.' , true);
			}
			$("#send_message").removeAttr('disabled');
		})
		.error(function() {
			$("#txtNewMessage").val(newMessage.messageContents);
			$("#txtNewMessage").removeAttr('disabled');
			shownotification('Message sending failed. Please try again.' , true);
		});
	popup.selectedconversation = storeConversation(newMessage);
	reload_popup_sms();
	showConversation(popup.selectedconversation);
	$("#txtNewMessage").val('');
};

shownotification = function(msg, isError, time) {
	if (!time) {
		time = 500;
	}
	var notifier = $('#notify');
	notifier.text(msg);

	if(isError) {
		notifier.removeClass().addClass('notification error');
	} else {
		notifier.removeClass().addClass('notification info');
	}
	notifier.hide();
	notifier.show('clip', null, 500, function() {
		setTimeout(function() {
			notifier.fadeOut();
		}, time);
	} );
};

refresh_conversation_view_ = function(conversation) {
	var messages = JSON.parse(localStorage[conversation]);
	$("#message_view").html('');
	for (var i in messages) {
		
		var msg = messages[i];
		if (msg.type == "NULL_SMS") {
			continue;
		}
		var clazz = 'incomingmessage';
		if (msg.sent_by_me) {
			clazz = 'outgoingmessage';
		}
		
		$("#message_view").append(
		  '<div class="' + clazz + '"><span>' + msg.messageContents +
		  '</span><br/><p style="font-size: 9px;">'+ formatTime(msg.time) +'<p/></div>'
		);
		
	}
	$("#message_view").scrollTop($("#message_view")[0].scrollHeight);
};

setupSocialLinks = function() {
	$('.facebook').on("click", function(e) {
		e.preventDefault();
		chrome.tabs.create({"url": 'https://www.facebook.com/Deskphone'});
	});
	$('.google').on("click", function(e) {
		e.preventDefault();
		chrome.tabs.create({"url": 'https://plus.google.com/u/0/114434273403374664847/'});
	});
	$('.email').on("click", function(e) {
		e.preventDefault();
		chrome.tabs.create({"url": 'mailto:info@deskphone.mobi'});
	});
};

// Returns true if it contains only valid phone number characters.
isAPhoneNumber = function(number) {
	try {
		e164 = formatE164(popup.countrycode, number);
		console.log(e164);

		if (!e164 || e164.trim().length == 0) {
			return false;
		}
		e164 = e164.substring(1);

		return e164 >=0 || e164 <=0;
	} catch (e) {
		return false;
	}
};

formatTime = function(t) {
	var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "June",
	                   "July", "Aug", "Sep", "Oct", "Nov", "Dec" ];

	var other = new Date(t);
	var now = new Date();
	
	var date ;

	if (now.getMonth() == other.getMonth() && now.getDate() == other.getDate()) {
		date = "Today";
	} else {
		date = monthNames[other.getMonth()] + ' ' + other.getDate();
	}

	var hours = other.getHours() > 12 ? other.getHours() - 12 : other.getHours();
	var timeString = other.getHours() > 12 ? "PM" : "AM";

	if (hours < 10) {
		hours = '0' + hours;
	}
	
	minutes = other.getMinutes();
	if (minutes < 10) {
		minutes = '0' + minutes;
	}

	return hours + ":" + minutes +" " + timeString + " "+ date;
};
