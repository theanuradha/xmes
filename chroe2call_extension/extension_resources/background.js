lastheartbeat = null;
popup_window_id = null;

channel = {};

channel.instance = null;

channel.Socket = function() {
	this.channelKey = null;
	this.connected = false;
	this.channel = null;
	this.socket = null;
	this.network_down = false;
	this.initiating_connection = false;
	this.loggedIn = false;
	channel.instance = this;
};

channel.Socket.prototype.isConnected = function() {
	return channel.instance.connected;
};

channel.Socket.prototype.isLoggedIn = function() {
	return channel.instance.loggedIn;
};

channel.Socket.prototype.connect = function() {
	if (!channel.instance.connected || !channel.instance.network_down || !channel.instance.initiating_connection) {
		channel.instance.initiating_connection = true;
		console.log('Creating new channel.');
		var url = environment.establish_channel;
		if (channel.instance.channelKey) {
			url += '?PREVIOUS_CHANNEL_ID=' + channel.instance.channelKey;
		}
		$.get(url)
			.success(channel.instance.handle_connect_response_)
			.error(channel.instance.noNetwork_);
	}
};

channel.Socket.prototype.noNetwork_ = function() {
	console.log('Network error. Re-trying in 30 seconds');
	channel.instance.network_down = true;
	setTimeout(function() {
		if (navigator.onLine) {
			channel.instance.network_down = false;
			channel.instance.connect();
		} else {
			channel.instance.noNetwork_();
			console.log('Network still down. Re-trying in 30 seconds');
		}}, 30000);
};



channel.Socket.prototype.handle_connect_response_ = function(data) {
	if (data && data.messageType == "NOT_LOGGED_IN") {
		console.log('User not logge in yet. Try later.');
		channel.instance.loggedIn = false;
		channel.instance.connected = false;
		channel.instance.initiating_connection = false;

		showNotification(
				'Click on the extension to log in.',
				'Not logged In',
				'icon_large.png');
	} else {
		$('#wcs-iframe').remove();
		channel.instance.loggedIn = true;
		channel.instance.channel = new goog.appengine.Channel(data.msg);
		channel.instance.channelKey = data.msg2;
		channel.instance.socket = channel.instance.channel.open();
		
		var channelSocket = channel.instance;

		channel.instance.socket.onopen = function() {
			lastheartbeat = new Date().getTime();
			channelSocket.connected = true;
			channelSocket.initiating_connection = false;
	    	console.log('Channel Opened.');
	    };
	    channel.instance.socket.onmessage = function(incoming) {
	    	lastheartbeat = new Date().getTime();
	    	var data = JSON.parse(incoming.data);
	    	if (data.type == 'MISSED_CALL') {
	    		onMissedCall(incoming);
		    } else if (data.type == 'INCOMING_SMS') {
		    	onIncomingSms(incoming);
		    } else {
		    	if (data.type == 'TEXT_MESSAGE' && data.msg == 'KEEP_ALIVE') {
		    		console.log('Received heartbeat from server.');
		    	}
		    }
	    };
	    channel.instance.socket.onerror = function(e) {
	    	// Wait for few seconds to see its really not connected and retry.
	    	console.log('Error occured: ' + e.description);
	    	channelSocket.destroy_();
	    	setTimeout(function() {
	    		if (!channelSocket.connected) {
	    			channelSocket.connect();
	    		}
	    	}, 10000);
	    };
	    channel.instance.socket.onclose = function(e) {
	    	console.log('Server closed the connection.');
	    	channelSocket.destroy_();
	    	setTimeout(function() {
	    		if (!channelSocket.connected) {
	    			channelSocket.connect();
	    		}
	    	}, 5000);
	    };
	}
};

channel.Socket.prototype.destroy_ = function() {
	channel.instance.connected = false;
	channel.instance.socket = null;
	channel.instance.channel = null;
	$('#wcs-iframe').remove();
};



var keepTryingUntilLoggedIn = function(timeout, c) {
	console.log('Checking if channel established.');
	setTimeout(function() {
		if (!channel.instance.isLoggedIn()) {
			console.log('Not logged in yet. Will check back in ' + timeout);
			channel.instance.connect();

			// Only try for 5 minutes to prevent getting stuck in the loop.
			if (timeout < 5 * 60 * 1000) {
				keepTryingUntilLoggedIn(timeout * 1.3);
			}
		}
	}, timeout);
};
$(document).ready(
		function() {
			var c = new channel.Socket();
			chrome.extension.onMessage.addListener(function(request, sender,
					sendResponse) {
				if (request.status == 'loggingin') {
					keepTryingUntilLoggedIn(10000, c);
				} else if (request.status == 'update_ontacts') {
					version = request.version;
					syncContacts(version, request.country);
				}
			});

			c.connect(false);

			lastheartbeat = new Date().getTime();
			heartbeat();
		});

showNotification = function(msg, title, icon) {
	var notification = webkitNotifications.createNotification(
			  icon,
			  title,
			  msg
			);
	notification.onclick = function() {
		if (popup_window_id) {
			try {
				chrome.windows.remove(popup_window_id, function(){});
			}catch(e) {
				console.log(e);
			}
 		}
		chrome.windows.create({url: 'newage.html', width: 500, height: 310, type: "detached_panel"},
				function(window) {
			popup_window_id = window.id;
		});};
	notification.show();
};

heartbeat = function() {
	// If last heartbeat is more than 4 minutes, send a keepalive request to server.
	now = new Date().getTime();
	timesincelast = now - lastheartbeat;
	MINUETES = 60 * 1000;
	console.log('Time since last heartbeat: ' + timesincelast/1000 + 'Seconds');
	if (timesincelast > 4*MINUETES && navigator.onLine) {
		if (!channel.instance.channelKey) {
			// No previous channel found. Try to establish a fresh connection.
			channel.instance.socket.onerror({description: "No news from server"});
			return;
		}
		$.get(environment.heartbeat + '?PREVIOUS_CHANNEL_ID=' + channel.instance.channelKey)
		 .success(function() {
			 console.log('Heartbeat request sent. Will check the status in 60 seconds.');
			 setTimeout(function() {
				 // If we still havent received anything from server. time to start a new connection.
				 now = new Date().getTime();
				 timesincelast = now - lastheartbeat;
				 if (timesincelast > 4*MINUETES) {
					 channel.instance.socket.onerror({description: "No news from server"});
				 }
			 }, 1*MINUETES);
		 });
	} else if (!navigator.onLine) {
		// Not doing anything.
	} else {
		// We are good. check back in 3 minutes.
		lastheartbeat = new Date().getTime();
	}
	setTimeout(heartbeat, 3*MINUETES);
};

onMissedCall = function (data) {
	var alertInfo = JSON.parse(data.data);
	var at = new Date(alertInfo.time);
	var from = alertInfo.no;
	if (alertInfo.contactName != '?') {
		from = alertInfo.contactName + '(' + alertInfo.no + ')';
	}
	storeMissedCall(alertInfo);
	storerecentevent('missedcall', alertInfo);
	showNotification(
			'At ' + at.toLocaleTimeString() + ' On your ' + alertInfo.fromPhone,
			'Missed Call from: ' + from, 'icon_large.png');
};

onIncomingSms = function (data) {
	var alertInfo = JSON.parse(data.data);
	var from = alertInfo.no;
	if (alertInfo.contactName != '?') {
		from = alertInfo.contactName + '(' + alertInfo.no + ')';
	}
	storeConversation(alertInfo);
	storerecentevent('sms', alertInfo);
	showNotification(
			alertInfo.messageContents,
			'New SMS from: ' + from, 'icon_large.png');
};
