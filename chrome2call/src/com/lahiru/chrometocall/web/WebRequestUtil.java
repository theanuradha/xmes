package com.lahiru.chrometocall.web;

import javax.servlet.http.HttpServletRequest;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.lahiru.chrometocall.guice.WebAppliationModule;

public class WebRequestUtil {
	//If local, the hostname + port needs to be prefixed.
	public static String getDomainPrefix(HttpServletRequest request) {
		if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Development) {
			return "http://" + request.getLocalName() + ":" + request.getLocalPort();
		} else {
			return "";
		}
	}

	public static String getLoginUrl(HttpServletRequest request) {
		UserService userService = UserServiceFactory.getUserService();
		String loginURL = getDomainPrefix(request)
				+ userService.createLoginURL(request.getRequestURI());

		return loginURL;
	}

	public static String getLogoutUrl(HttpServletRequest request) {
		UserService userService = UserServiceFactory.getUserService();
		String logoutURL = getDomainPrefix(request)
				+ userService.createLogoutURL(WebAppliationModule.WEB_INDEX);

		return logoutURL;
	}

}
