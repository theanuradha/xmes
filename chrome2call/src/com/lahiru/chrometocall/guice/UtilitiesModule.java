package com.lahiru.chrometocall.guice;

import java.util.logging.Logger;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheManager;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

/**
 * Bind various utility objects.
 * @author lahiru
 */
public class UtilitiesModule extends AbstractModule {
	Logger log = Logger.getLogger(UtilitiesModule.class.getName());

	@Override
	protected void configure() {
	}

	@Provides
	Gson provideGsonParser() {
		return new GsonBuilder().disableHtmlEscaping().create();
	}

	@Provides
	User provideCurrentUser() {
		return UserServiceFactory.getUserService().getCurrentUser();
	}

	@Provides
	Cache provideMemChache() {
		try {
			return CacheManager.getInstance().getCacheFactory().createCache(ImmutableMap.of());
		} catch (CacheException e) {
			log.warning("Could not create the application cache: " + e);
			return null;
		}

	}
}
