package com.lahiru.chrometocall.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 * Configure all the web modules here.
 * @author lahiru
 */
public class MyGuiceServletContextListener extends GuiceServletContextListener{

	@Override protected Injector getInjector() {
		return Guice.createInjector(
				new WebAppliationModule(),
				new UtilitiesModule(),
				new ServiceModule());
	}
}
