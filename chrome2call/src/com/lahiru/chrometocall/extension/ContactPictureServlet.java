package com.lahiru.chrometocall.extension;
import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.service.ContactsService;

/**
 * Returns entire set of user configurations. Such as registered devices Etc.
 * @author Lahiru
 */
@SuppressWarnings("serial")
public class ContactPictureServlet extends HttpServlet {
	@Inject Provider<ContactsService> contactServiceProvider;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String contactId = req.getParameter("contactID");

		if (contactId != null) {
			byte[] image = contactServiceProvider.get().getContactImage(Long.parseLong(contactId));
			resp.setContentType("image/jpeg");
			resp.getOutputStream().write(image);
		} else {
			throw new RuntimeException();
		}
	}
}
