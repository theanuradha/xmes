package com.lahiru.chrometocall.extension;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.json.ContactMessageToClient;
import com.lahiru.chrometocall.service.ContactsService;
import com.lahiru.chrometocall.service.UserInfoService;

/**
 * Returns all the contacts for the user which has been updated since users current version.
 * @author Lahiru
 */
@SuppressWarnings("serial")
public class GetContactsServlet extends HttpServlet {

	private final Gson gson;
	@Inject Provider<ContactsService> contactService;
	@Inject Provider<UserInfoService> userInfoService;

	@Inject
	public GetContactsServlet(Gson gson) {
		this.gson = gson;
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		try {
			String currentVersion = req.getParameter("version");
			long version = 0;
			if (!Strings.isNullOrEmpty(currentVersion)) {
				version = Long.parseLong(currentVersion);
				List<ContactMessageToClient> contacts = contactService.get()
						.getContactsAfterVersion(
								userInfoService.get().currentUserEmail(),
								version);

				resp.setContentType("application/json");
				resp.getWriter().write(gson.toJson(contacts));
			} else {
				throw new BaseException("Current version not specified");
			}
		} catch (Exception ex) {
			throw new BaseException(ex);
		}
	}
}
