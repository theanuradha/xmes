package com.lahiru.chrometocall.extension;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.lahiru.chrometocall.service.UserInfoService;
import com.lahiru.chrometocall.servlet.StaticContentHostServlet;

/**
 * Register the user in the system, if new.
 * 
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class UserRegistrationServlet extends HttpServlet {
	private final Provider<UserInfoService> userInfoServiceProvider;

	@Inject
	public UserRegistrationServlet(Provider<UserInfoService> userInfoServiceProvider) {
		this.userInfoServiceProvider = userInfoServiceProvider;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String location = req.getHeader("X-AppEngine-CityLatLong");
		userInfoServiceProvider.get().registerIfNew(location, true);

		// Send to log-in successful page.
		resp.sendRedirect(StaticContentHostServlet.SUCCESSFUL_LOGGEDIN_PAGE);
	}

}
