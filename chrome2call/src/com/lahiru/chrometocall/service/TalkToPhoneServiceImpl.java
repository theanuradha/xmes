package com.lahiru.chrometocall.service;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.mail.search.MessageIDTerm;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.appengine.api.users.User;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.google.inject.Inject;
import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.json.OutgoingSmsRequest;
import com.lahiru.chrometocall.common.json.PingAcknowledgement;
import com.lahiru.chrometocall.entitiy.ApplicationUser;
import com.lahiru.chrometocall.entitiy.MobileDevice;
import com.lahiru.chrometocall.entitiy.SMS;
import com.lahiru.chrometocall.service.da.ApplicationUserManager;
import com.lahiru.chrometocall.service.da.MessageManager;

public class TalkToPhoneServiceImpl implements TalkToPhoneService {
	private static final String EXTENSION_ANDROIDAPP_NOTINSTALLED = "Deskphone not installed on device";
	public static final String RESPONCE_FAIL = "FAIL";
	public static final String RESPONCE_OK = "OK";

	Logger log = Logger.getLogger(TalkToPhoneServiceImpl.class.getName());

	private static final String SENDER_ID = "AIzaSyCRNDjlEXQSuQoOvPlSteQo6pDtAx3Vdm8";
	@Inject @Nullable User currentUser;
	@Inject ApplicationUserManager userManager;
	@Inject MessageManager messageManager;

	@Override
	public String placeCall(String deviceId, CallRequest request) {
		assertSessionExists();

		String phoneNumber = request.getPhoneNumber();

		MobileDevice device = lookupDeviceByUser(deviceId);

		// Validate phone number.
		boolean isValid = true;
		PhoneNumberUtil numberUtil = PhoneNumberUtil.getInstance();
		try {
			PhoneNumber number = numberUtil.parse(phoneNumber, device.getCountry().toUpperCase());
			isValid = numberUtil.isValidNumber(number);
		} catch (NumberParseException e1) {
			isValid = false;
		}

		if (!isValid) {
			return "Invalid Phone Number: " + phoneNumber;
		}

		Message message = new Message.Builder().addData("number", phoneNumber)
				.addData("type", "callrequest").addData("note", request.getNote())
				.addData("time", request.getRequestTime()).build();

		return sendMessage(device, message);
	}

	protected String sendMessage(MobileDevice device,
			Message message) {
		Sender sender = new Sender(SENDER_ID);
		try {
			Result result = sender.send(message, device.getC2dmRegistrationId(), 5);

			boolean invalidDevice = result.getMessageId() != null
					&& Constants.ERROR_NOT_REGISTERED.equals(result.getErrorCodeName());

			if (invalidDevice || device.isMarkedForDeletion()) {
				return EXTENSION_ANDROIDAPP_NOTINSTALLED;
			}

			return RESPONCE_OK;
		} catch (IOException e) {
			throw new BaseException(
					"Could not send call request " + currentUser.getEmail() + "/" + device.getRegistrationId(), e);
		}
	}

	@Override
	public String sendPingRequest(String c2dmId, boolean isAuthenticated, String email) {
		Sender sender = new Sender(SENDER_ID);
		try {
			Message message = new Message.Builder()
	 		 .addData("type", "ping")
	 		 .addData("email", email)
	 		 .build();
			Result result = sender.send(message, c2dmId, 5);

			// Remind user to authenticate the device.
			if (!isAuthenticated) {
				Message message2 = new Message.Builder()
		 		 .addData("type", "adminmessage")
		 		 .addData("reaction", "url")
		 		 .addData("reaction_param1", "http://deskphone.mobi")
		 		 .addData("message", "Your device is not active. Click here to activate now.")
		 		 .build();
				sender.send(message2, c2dmId, 5);
			}

			if (result.getMessageId() != null) {
				String canonicalRegistrationId = result.getCanonicalRegistrationId();
				if (canonicalRegistrationId != null) {
					log.warning("Canonical registration found for : " + email);
					return canonicalRegistrationId;
				} else {
					return RESPONCE_OK;
				}
			} else {
				String errorCodeName = result.getErrorCodeName();
				log.warning("Error code returned for " + email + " Error: " + errorCodeName + ":" + result.getMessageId());
				if (Constants.ERROR_NOT_REGISTERED.equals(errorCodeName)) {
					return RESPONCE_FAIL;
				} else {
					return RESPONCE_OK; // In reality this is an error. But we dont know how to deal with it right now.
				}
			}
		} catch (IOException e) {
			throw new BaseException(
					"Could not send call request to device with ID: " + c2dmId);
		}
	}

	@Override
	public String setAccountStatus(String deviceId, boolean isActive) {
		assertSessionExists();

		MobileDevice device = lookupDeviceByUser(deviceId);

		 Sender sender = new Sender(SENDER_ID);
		 Message message = new Message.Builder()
		 		 .addData("type", "activationStatus")
		     .addData("email", currentUser.getEmail())
		     .addData("isActive", Boolean.toString(isActive))
		     .build();
		 try {
			sender.send(message, device.getC2dmRegistrationId(), 5);
			return RESPONCE_OK;
		} catch (IOException e) {
			throw new BaseException(
					"Could not send activation state change");
		}
	}

	private MobileDevice lookupDeviceByUser(String deviceId, String email) {
		ApplicationUser user = userManager.lookupUserByEmailAddress(email);
		MobileDevice device = getDevice(deviceId, user);
		if (device == null) {
			throw new BaseException("Device " + deviceId + "not found.");
		}
		return device;
	}

	private MobileDevice lookupDeviceByUser(String deviceId) {
		return lookupDeviceByUser(deviceId, currentUser.getEmail());
	}

	private MobileDevice getDevice(String deviceId, ApplicationUser user) {
		for (MobileDevice device : user.getDevices()) {
			if (deviceId.equals(device.getRegistrationId())) {
				return device;
			}
		}

		return null;
	}

	private void assertSessionExists() {
		if (currentUser == null) {
			throw new BaseException ("Not a valid operation. User must log in with a google account");
		}
	}

	@Override
	public String sendSms(OutgoingSmsRequest r) throws BaseException {
		assertSessionExists();
		MobileDevice device = lookupDeviceByUser(r.getPhoneId());

		Long msgID;
		try {
			SMS msg = messageManager.saveMessage(r);
			msgID = msg.getId();
		}catch (Exception ex) {
			// Saving is trivial. Can be ignored.
			log.warning("could not save outgoing message. Delivery status will not be available.");
			msgID = -1L;
		}

		Message message = new Message.Builder()
			.addData("message", r.getMessageContents())
			.addData("no", r.getNo())
			.addData("time", Long.toString(r.getTime()))
			.addData("type", "sendsms")
			.addData("id", Long.toString(msgID))
			.build();

		return sendMessage(device, message);
	}

	@Override
	public void updateDeviceVersion(PingAcknowledgement ack) throws BaseException {
		ApplicationUser user = userManager.lookupUserByEmailAddress(ack.getEmail());

		int index = getDeviceIndexForUser(ack, user);

		if (index != -1) {
			Date lastUpdateTime = new Date(System.currentTimeMillis());
	
			MobileDevice device = user.getDevices().get(index);
			device.setDpVersion(Integer.parseInt(ack.getDpVersion()));
			device.setLastHeardOn(lastUpdateTime);
			device.setMarkedForDeletion(false);
	
			userManager.saveUser(user);
		}
	}

	private int getDeviceIndexForUser(PingAcknowledgement ack, ApplicationUser user) {
		int index = -1;
		for (int i= 0; i< user.getDevices().size(); i++) {
			MobileDevice device = user.getDevices().get(i);
			if (device.getRegistrationId().equals(ack.getPhoneId())) {
				index = i;
				break;
			}
		}
		return index;
	}
}
