package com.lahiru.chrometocall.service;

import java.util.List;

import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.json.ContactMessage;
import com.lahiru.chrometocall.common.json.ContactMessageToClient;

public interface ContactsService {
	/**
	 * Saves the list of contacts for all the given users. This is an atomic operation.
	 * A {@link BaseException} will be thrown if at least one of the saves failed.
	 * @param userEmails
	 * @param country 
	 * @param contactDetails
	 */
	void syncCotacts(List<String> userEmails, String deviceId, List<ContactMessage> contactDetails);

	byte[] getContactImage(long parseLong);

	/**
	 * Returns contacts created for the current user after the given version.
	 * @param version
	 * @return
	 */
	List<ContactMessageToClient> getContactsAfterVersion(String email, long version);
}
