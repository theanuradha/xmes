package com.lahiru.chrometocall.service;

import com.lahiru.chrometocall.common.e.BaseException;
import com.lahiru.chrometocall.common.json.OutgoingSmsRequest;
import com.lahiru.chrometocall.common.json.PingAcknowledgement;

/**
 * Use this service to communicate with android phones via
 * Google's cloud messaging service.
 * 
 * @author lahiruw
 *
 */
public interface TalkToPhoneService {
	/**
	 * Rquest to initiate a phone call for the given device of the current logged in user.
	 * @param device unique device id.
	 */
	String placeCall(String device, CallRequest request) throws BaseException;

	/**
	 * Send an sms send request to phone.
	 * @param request Request details
	 */
	String sendSms(OutgoingSmsRequest request) throws BaseException;

	/**
	 * Update device with the current version code and set current time to last
	 * heard time.
	 * 
	 * @param ack
	 * @throws BaseException
	 */
	void updateDeviceVersion(PingAcknowledgement ack) throws BaseException;

	public static class CallRequest {
		private final String phoneNumber;
		private final String note;
		private final String requestTime;

		public CallRequest(String phoneNumber, String note, String requestTime) {
			this.phoneNumber = phoneNumber;
			this.note = note;
			this.requestTime = requestTime;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public String getNote() {
			return note;
		}
		public String getRequestTime() {
			return requestTime;
		}

	}

	/**
	 * Notify the device about the activation state change.
	 * @param deviceId
	 * @param isActive
	 * @return
	 */
	String setAccountStatus(String deviceId, boolean isActive);

	String sendPingRequest(String c2dmId, boolean isActive, String email);
}
