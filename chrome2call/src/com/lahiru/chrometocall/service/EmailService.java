package com.lahiru.chrometocall.service;

public interface EmailService {
	void sendNewMobileDeviceRegistrationEmail(String email);
}
