package com.lahiru.chrometocall.service.da;

import com.lahiru.chrometocall.common.json.IncomingSmsAlert;
import com.lahiru.chrometocall.common.json.OutgoingSmsRequest;
import com.lahiru.chrometocall.entitiy.SMS;

public interface MessageManager {
	public enum MessageType {
		INCOMING, OUTGOING
	}

	SMS saveMessage(IncomingSmsAlert alert);
	SMS saveMessage(OutgoingSmsRequest request);
}
