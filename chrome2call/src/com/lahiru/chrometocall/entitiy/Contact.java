package com.lahiru.chrometocall.entitiy;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

@Entity
public class Contact implements Serializable {
	private static final long serialVersionUID = -4263709275874638309L;

	// contactgroup id _ contact id
	@Id
	private Long id;
	@Parent
	private Key<Person> person;
	@Unindexed private Key<ContactImage> imgage;
	@Unindexed
	private String name;
	private Long contactGroupId;
	@Unindexed
	private String type;
	private String phoneNumber;
	private Long version;
	@Unindexed private String phoneId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Key<Person> getPerson() {
		return person;
	}

	public void setPerson(Key<Person> person) {
		this.person = person;
	}

	public Key<ContactImage> getImgage() {
		return imgage;
	}

	public void setImgage(Key<ContactImage> imgage) {
		this.imgage = imgage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getContactGroupId() {
		return contactGroupId;
	}

	public void setContactGroupId(Long contactGroupId) {
		this.contactGroupId = contactGroupId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}

	public static class Builder {
		private Long id;
		private Key<Person> person;
		private Key<ContactImage> imgage;
		private String name;
		private Long contactGroupId;
		private String type;
		private String phoneNumber;
		private Long version;
		private String phoneId;

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder person(Key<Person> person) {
			this.person = person;
			return this;
		}

		public Builder imgage(Key<ContactImage> imgage) {
			this.imgage = imgage;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder contactGroupId(Long contactGroupId) {
			this.contactGroupId = contactGroupId;
			return this;
		}

		public Builder type(String type) {
			this.type = type;
			return this;
		}

		public Builder phoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
			return this;
		}

		public Builder version(Long version) {
			this.version = version;
			return this;
		}

		public Builder phoneId(String phoneId) {
			this.phoneId = phoneId;
			return this;
		}

		public Contact build() {
			Contact contact = new Contact();
			contact.id = id;
			contact.person = person;
			contact.imgage = imgage;
			contact.name = name;
			contact.contactGroupId = contactGroupId;
			contact.type = type;
			contact.phoneNumber = phoneNumber;
			contact.version = version;
			contact.phoneId = phoneId;
			return contact;
		}
	}
}
