package com.lahiru.chrometocall.common.e;

/**
 * Every unrecoverable exception should be a subclass of this. And these type of
 * exception's should be handled last.
 * 
 * Do not use for recoverable exceptions.
 * @author lahiruw
 *
 */
@SuppressWarnings("serial")
public class BaseException extends RuntimeException {

	public BaseException(String errorMessage, Throwable t) {
		super(errorMessage, t);
	}

	public BaseException(String errorMessage) {
		super(errorMessage);
	}

	public BaseException(Throwable t) {
		super(t);
	}
	
}
