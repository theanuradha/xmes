package com.lahiru.chrometocall.common.json;

public abstract class PhoneAlert {
	private String[] emails;
	private long time;
	private String fromPhone;
	private String type;
	private String phoneId = "";

	public String[] getEmails() {
		return emails;
	}

	public long getTime() {
		return time;
	}

	public String getFromPhone() {
		return fromPhone;
	}

	public String getType() {
		return type;
	}

	public void setEmails(String[] emails) {
		this.emails = emails;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public void setFromPhone(String fromPhone) {
		this.fromPhone = fromPhone;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}
}
