package com.lahiru.chrometocall.common.json;

public class OutgoingSmsRequest extends PhoneAlert {
	private String no;
	private String contactName;
	private String messageContents;
	private boolean sent_by_me;

	public OutgoingSmsRequest() {
	}

	public String getNo() {
		return no;
	}

	public String getContactName() {
		return contactName;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getMessageContents() {
		return messageContents;
	}

	public void setMessageContents(String messageContents) {
		this.messageContents = messageContents;
	}

	public boolean isSent_by_me() {
		return sent_by_me;
	}

	public void setSent_by_me(boolean sent_by_me) {
		this.sent_by_me = sent_by_me;
	}
}