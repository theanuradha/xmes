syncConversationToSite = function (conversation) {
	var prefix = "C_";
	var conversationList = localStorage.conversations;
	
	var key = prefix + conversation.phoneId + "_" + conversation.no.substring(1);
	var existing = localStorage[key];
	if (!existing) {
		existing = new  Array();
	} else {
		existing = JSON.parse(existing);
		// If we have a different contact name, update it now.
		if (conversation.contactName != '?') {
			for (var i in existing) {
				existing[i].contactName = conversation.contactName;
			}
		}
	}
	existing.push(conversation);
	
	localStorage.setItem(key, JSON.stringify(existing));

	if(!conversationList) {
		conversationList = new Array();
	} else {
		conversationList = JSON.parse(conversationList);
	}

	if (conversationList.indexOf(key) != -1) {
		conversationList.splice(conversationList.indexOf(key), 1);
	}

	conversationList.push(key);
	localStorage.setItem("conversations", JSON.stringify(conversationList));

	return key;
};

syncMissedCallToSite = function (missedCall) {
	var missedCalls = localStorage.missedCalls;
	if (!missedCalls) {
		missedCalls = new Array();
	} else {
		missedCalls = JSON.parse(missedCalls);
	}

	missedCalls.unshift(missedCall);
	localStorage.missedCalls = JSON.stringify(missedCalls);
};
