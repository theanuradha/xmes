package com.lahiru.xmes.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.lahiru.xmes.data.DPContact.ContactInfo;
import com.lahiru.xmes.data.DPContact.ContactType;

public class ContactsDataHandler extends SQLiteOpenHelper {
	private static ContactsDataHandler instance;

	public static ContactsDataHandler i(Context context) {
		if (instance == null) {
			instance = new ContactsDataHandler(context);
		}
		return instance;
	}

	private static final String DB_NAME = "Deskphone_contacts";
	private static final int VERSION = 1;
	private static final String TABLE_CONTACTS_GROUP = "ContactsGroup";
	private static final String ID_COLUMN = "_id";
	private static final String CONTACT_NAME_COLUMN = "contact_name";
	private static final String CONTACT_THUMBNAIL_COLUMN = "contact_thumbnail";
	private static final String TABLE_CONTACT = "contact";
	private static final String CONTACT_GROUP_ID_COLUMN = "groupid";
	private static final String CONTACT_TYPE = "contact_type";
	private static final String CONTACT_VERSION = "version";
	private static final String CONTACT_NUMBER = "number";
	private static final String CONTACT_MD5 = "md5";
	private static final String CONTACT_DIRTY = "dirty";
	
	private static int ID_COLUMN_INDEX  = 0;
	private static int CONTACT_NAME_COLUMN_INDEX = 1;
	private static int CONTACT_THUMBNAIL_COLUMN_INDEX = 2;
	@SuppressWarnings("unused")
	private static int CONTACT_MD5_INDEX = 3;
	private static int CONTACT_DIRTY_INDEX = 4;
	@SuppressWarnings("unused")
	private static int CONTACT_GROUP_ID_COLUMN_INDEX = 1;
	private static int CONTACT_TYPE_INDEX = 2;
	private static int CONTACT_VERSION_INDEX = 3;
	private static int CONTACT_NUMBER_INDEX = 4;

	private static final String LOOKUP_CONTACTGROUP_BYID = "SELECT * FROM "
			+ TABLE_CONTACTS_GROUP + " WHERE " + ID_COLUMN + "= ?";
	private static final String LOOKUP_MODIFIED_CONTACTGROUP = "SELECT * FROM "
			+ TABLE_CONTACTS_GROUP + " WHERE " +  CONTACT_DIRTY + "= ?";
	private static final String LOOKUP_CONTACTGROUP_BYID_MD5ONLY = "SELECT " + CONTACT_MD5 + " FROM "
			+ TABLE_CONTACTS_GROUP + " WHERE " + ID_COLUMN + "= ?";
	private static final String LOOKUP_CONTACT_BY_GROUP_ID = "SELECT * FROM "
			+ TABLE_CONTACT + " WHERE " + CONTACT_GROUP_ID_COLUMN + " = ?";
	private static final String LOOKUP_CONTACT_GROUP_ID_BY_NUMBER = "SELECT " + CONTACT_GROUP_ID_COLUMN + " FROM "
			+ TABLE_CONTACT + " WHERE " + CONTACT_NUMBER + " = ?";

	public ContactsDataHandler(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_GROUP_TABLE = "CREATE TABLE " + TABLE_CONTACTS_GROUP + "("
        + ID_COLUMN + " INTEGER PRIMARY KEY,"
				+ CONTACT_NAME_COLUMN + " TEXT,"
				+ CONTACT_THUMBNAIL_COLUMN + " BLOB,"
				+ CONTACT_MD5 + " INTEGER," 
				+ CONTACT_DIRTY + " INTEGER)";

		String CREATE_CONTACT_TABLE = "CREATE TABLE " + TABLE_CONTACT + "("
        + ID_COLUMN + " INTEGER PRIMARY KEY,"
        + CONTACT_GROUP_ID_COLUMN + " INTEGER,"
				+ CONTACT_TYPE + " TEXT,"
				+ CONTACT_VERSION + " INTEGER,"
				+ CONTACT_NUMBER + " TEXT" + ")";

		db.execSQL(CREATE_CONTACTS_GROUP_TABLE);
		db.execSQL(CREATE_CONTACT_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {}

	/**
	 * Update an existing contact. If doesnt't yet exists in the db will create
	 * a new record.
	 * 
	 * @param contact
	 */
	public boolean storeContact(DPContact contact) {
		if (!isUpdated(contact.getId(), contact.hashCode())) {
			return false;
		}

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues fromDPContact = fromDPContact(contact);
		int update = db.update(
				TABLE_CONTACTS_GROUP, 
				fromDPContact, ID_COLUMN + " = ?", 
				new String[] { Long.toString(contact.getId())});

		// Insert if no row has been updated.
		if(update < 1) {
			db.insert(TABLE_CONTACTS_GROUP, null, fromDPContact);
		}

		// Now save each contact info record.
		for (ContactInfo contactInfo : contact.getContactInfo()) {
			ContentValues fromContactInfo = fromContactInfo(contactInfo, contact.getId());
			update = db.update(
					TABLE_CONTACT,
					fromContactInfo, ID_COLUMN + " = ?",
					new String[] {Long.toString(contactInfo.get_id())});

			if (update < 1) {
				db.insert(TABLE_CONTACT, null, fromContactInfo);
			}
		}

		db.close();
		return true;
	}

	/**
	 * Returns the contact identified by the given id. Returns null if not found.
	 * It will also populate all the contact info fields.
	 */
	public DPContact getContactById(long id) {
		DPContact result = null;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cgCur = db.rawQuery(LOOKUP_CONTACTGROUP_BYID, new String[] {Long.toString(id)});

		if (cgCur.moveToFirst()) {
			result = toDPContact(cgCur, db);
		}

		cgCur.close();
		return result;
	}

	public List<DPContact> getDirtyContacts() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cgCur = db.rawQuery(LOOKUP_MODIFIED_CONTACTGROUP, new String[] {"1"});

		List<DPContact> results = new ArrayList<DPContact>();
		while (cgCur.moveToNext()) {
			results.add(toDPContact(cgCur, db));
		}

		cgCur.close();
		return results;
	}

	public void markNotDirty(List<DPContact> currentSet) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues setNotDirty = new ContentValues();
		setNotDirty.put(CONTACT_DIRTY, 0);
		for (DPContact current : currentSet) {
			db.update(TABLE_CONTACTS_GROUP, setNotDirty, ID_COLUMN + " = ?",
					new String[] { Long.toString(current.getId()) });
		}
		
		db.close();
	}

	public boolean isUpdated(long _id, int hashCode) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(LOOKUP_CONTACTGROUP_BYID_MD5ONLY, new String[] {Long.toString(_id)});

		if (c.moveToFirst()) {
			int md5 = c.getInt(0);
			c.close();
			return hashCode != md5;
		}

		c.close();
		// Needs updating means, the contact is new.
		return true;
	}

	public DPContact getByPhoneNumber(String e164Number) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor contactCursor = db.rawQuery(LOOKUP_CONTACT_GROUP_ID_BY_NUMBER, new String[] {e164Number});

		long grpId = -1;
		if (contactCursor.moveToFirst()) {
			grpId = contactCursor.getLong(0);
			return getContactById(grpId);
		} else {
			return null;
		}
	}

	private DPContact toDPContact(Cursor cur, SQLiteDatabase db) {
		DPContact contact = new DPContact(
				cur.getLong(ID_COLUMN_INDEX),
				cur.getString(CONTACT_NAME_COLUMN_INDEX),
				cur.getBlob(CONTACT_THUMBNAIL_COLUMN_INDEX));
		contact.setDirty(cur.getInt(CONTACT_DIRTY_INDEX) == 1);

		Cursor ciCursor = db.rawQuery(LOOKUP_CONTACT_BY_GROUP_ID, new String[] {Long.toString(contact.getId())});

		while (ciCursor.moveToNext()) {
			contact.getContactInfo().add(toContactInfo(ciCursor));
		}

		ciCursor.close();
		return contact;
	}

	private ContactInfo toContactInfo(Cursor ciCursor) {
		ContactInfo contactInfo = new ContactInfo(
				ciCursor.getInt(ID_COLUMN_INDEX),
				ContactType.valueOf(ciCursor.getString(CONTACT_TYPE_INDEX)),
				ciCursor.getString(CONTACT_NUMBER_INDEX),
				ciCursor.getInt(CONTACT_VERSION_INDEX));
		return contactInfo;
	}

	private ContentValues fromDPContact(DPContact contact) {
		ContentValues content = new ContentValues();

		content.put(CONTACT_NAME_COLUMN, contact.getName());
		content.put(CONTACT_THUMBNAIL_COLUMN, contact.getPicture());
		content.put(ID_COLUMN, contact.getId());
		content.put(CONTACT_MD5, contact.hashCode());
		content.put(CONTACT_DIRTY, contact.isDirty() ? 1 : 0);

		return content;
	}

	private ContentValues fromContactInfo(ContactInfo contactInfo, long groupId) {
		ContentValues content = new ContentValues();

		content.put(ID_COLUMN, contactInfo.get_id());
		content.put(CONTACT_GROUP_ID_COLUMN, groupId);
		content.put(CONTACT_NUMBER, contactInfo.getPhoneNumber());
		content.put(CONTACT_VERSION, contactInfo.getVersion());
		content.put(CONTACT_TYPE, contactInfo.getType().toString());

		return content;
	}
}
