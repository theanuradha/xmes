package com.lahiru.xmes;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.TimePicker;

import com.lahiru.xmes.data.CallRequest;
import com.lahiru.xmes.data.CallRequestDataHandler;
import com.lahiru.xmes.service.TelephonyService;

public abstract class SchedulableCallActivity extends AbstractActivity {
	protected Calendar lastSetTime;
	protected static final int TIME_DIALOG_ID = 10001;

	protected void scheduleCall(long nextTime, CallRequest s) {
		s.setTime(nextTime);
		s.setType(CallRequest.TYPE_SCHEDULED);
	
		Intent notifierService = new Intent(this, CallNotificationService.class);
		notifierService.putExtra("cr", s.get_id());
		PendingIntent pintent = PendingIntent.getService(
				this, Integer.parseInt(s.get_id()), notifierService, PendingIntent.FLAG_ONE_SHOT);
		AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		alarm.set(AlarmManager.RTC_WAKEUP, nextTime, pintent);
	
		CallRequestDataHandler.INSTANCE(this).update(s);
		TelephonyService.updateScheduledCallNotification(this);
	}

	protected final TimePickerDialog.OnTimeSetListener timePickerListener = 
			new TimePickerDialog.OnTimeSetListener() {
	
		@SuppressLint("SimpleDateFormat")
		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
			Calendar setTime = GregorianCalendar.getInstance();
			Calendar currentTime = GregorianCalendar.getInstance();
			setTime.set(Calendar.HOUR_OF_DAY, selectedHour);
			setTime.set(Calendar.MINUTE, selectedMinute);

			if (setTime.before(currentTime)) {
				// Next day.
				setTime.add(Calendar.DAY_OF_MONTH, 1);
			}

			lastSetTime = setTime;
			getSetTimeButton().setText(new SimpleDateFormat("h:mm a").format(setTime.getTime()));
			getConfirmButton().setEnabled(true);
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		Calendar currentTime = (lastSetTime != null)
				? lastSetTime 
				:GregorianCalendar.getInstance();
		switch (id) {
		case TIME_DIALOG_ID:
			return new TimePickerDialog(this, timePickerListener,
					currentTime.get(Calendar.HOUR_OF_DAY), currentTime.get(Calendar.MINUTE),
					false);
 
		}
		return null;
	}

	protected abstract Button getSetTimeButton();
	protected abstract Button getConfirmButton();
}
