package com.lahiru.xmes;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;

import com.lahiru.xmes.service.TalkToServer;

public class ErrorHandler {
	public static void handleNonUiError(Throwable e) {
		print(e);
	}

	public static void handleUiError(Activity c, Exception e) {
		print(e);
		c.finish();
	}

	public static void handleUiError(Context c, Exception e) {
		print(e);
	}

	private static void print(Throwable e) {
		try {
			JSONObject requestParams = new JSONObject();
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			sw.toString();
			requestParams.put("error", e.getMessage());
			requestParams.put("details", sw);
			
			
				TalkToServer.forceClosed(requestParams.toString());
			} catch (Exception e1) {
				// Do nothing.
			}
	}
}
