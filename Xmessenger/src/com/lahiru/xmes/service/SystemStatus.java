package com.lahiru.xmes.service;

import android.app.KeyguardManager;
import android.content.Context;
import android.telephony.TelephonyManager;

public class SystemStatus {
	public static boolean isSystemLocked(Context c) {
		KeyguardManager kgMgr = (KeyguardManager) c.getSystemService(Context.KEYGUARD_SERVICE);
		return kgMgr.inKeyguardRestrictedInputMode();
	}

	public static boolean isOnACall(Context c) {
		TelephonyManager pm = getTelephonyManager(c);
		int status = pm.getCallState();

		return (status == TelephonyManager.CALL_STATE_OFFHOOK 
				|| status == TelephonyManager.CALL_STATE_RINGING);
	}

	public static TelephonyManager getTelephonyManager(Context c) {
		TelephonyManager pm = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
		return pm;
	}
}
