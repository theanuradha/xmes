package com.lahiru.xmes.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.lahiru.xmes.R;

public class ContactSyncService extends IntentService {
	private static final int SYNC_INTERVAL = 6 * 60 * 60 * 1000;
	private Context otherConetex;

	public ContactSyncService() {
		super("Contact Sync Service");
		this.otherConetex = this;
	}

	public ContactSyncService(Context otherConetex) {
		this();
		this.otherConetex = otherConetex;
	}

	@Override
	public void onHandleIntent(Intent arg0) {
		String status = ApplicationPreferences
				.getPreference(otherConetex, otherConetex.getString(R.string.pref_contact_sync_status));

		String lastSync = ApplicationPreferences
				.getPreference(otherConetex, otherConetex.getString(R.string.pref_contact_last_sync));

		long lastSyncTime = lastSync == null || lastSync.trim().length() == 0
				? 0
				: Long.parseLong(lastSync);
				
		boolean isSyncTime =  System.currentTimeMillis() - lastSyncTime > SYNC_INTERVAL;
		if (isSyncTime) {
			boolean needsServerUpdate = ContactsHelper.CONTACT_SYNC_STATUS_NEED_SERVER_SYNC.equals(status)
					| doFullSync();
			String newStatus = needsServerUpdate 
					? ContactsHelper.CONTACT_SYNC_STATUS_NEED_SERVER_SYNC
					: ContactsHelper.CONTACT_SYNC_STATUS_SYNC_COMPLETE;

			// Next time just do an update check.
			ApplicationPreferences.savePreference(
					otherConetex.getString(R.string.pref_contact_sync_status), 
					newStatus, otherConetex);
			ApplicationPreferences.savePreference(
					otherConetex.getString(R.string.pref_contact_last_sync), 
					Long.toString(System.currentTimeMillis()), otherConetex);
			if (needsServerUpdate) {
				if (updateServer()) {
					ApplicationPreferences.savePreference(
							otherConetex.getString(R.string.pref_contact_sync_status), 
							ContactsHelper.CONTACT_SYNC_STATUS_SYNC_COMPLETE, otherConetex);
				}
			}
		}
	}

	private boolean doFullSync() {
		boolean hasUpdates = ContactsHelper.i().syncAllContacts(otherConetex);
		return hasUpdates;
	}

	private boolean updateServer() {
		return ContactsHelper.i().udateContactsToServer(otherConetex);
	}

}
