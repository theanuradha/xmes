package com.lahiru.xmes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.lahiru.xmes.service.AccountService;
import com.lahiru.xmes.service.AccountService.XmesAccount;
import com.lahiru.xmes.service.DeviceManager;

public class UserRegistrationActivity extends Activity implements OnClickListener {
	private static final String TAG = "UserRegistrationActivity";

		private LinearLayout accountsPanel;
		private LinearLayout registeredAccountsPanel;
		private Button registerButton;
		private ProgressDialog progress;

		private Map<String, XmesAccount> accounts;
		
		private Handler uiHandler = new Handler();

		private Runnable updateUi = new Runnable() {
			@Override
			public void run() {
        try {
					renderAccountsList();
				} catch (Exception e) {
					handleError(e);
				}
			}
		};
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();

        Runnable r = new Runnable() {
					@Override
					public void run() {
						try {
							showProgressDialog("Initializing the appliation for first time use");
							DeviceManager.forContext(UserRegistrationActivity.this).registerIfNew();
							accounts = AccountService.getAvailableAccounts(UserRegistrationActivity.this);
							hideProgressDialog();
			        uiHandler.post(updateUi);
						} catch (Exception e) {
							handleError(e);
						}
					}
				};

				Thread initThread = new Thread(r);
				initThread.start();
    }

		private void init() {
			setContentView(R.layout.main);
			accountsPanel = (LinearLayout) findViewById(R.id.accounts_panel);
			registeredAccountsPanel = (LinearLayout) findViewById(R.id.registered_accounts_panel);
			registerButton = (Button) findViewById(R.id.register_button);

			registerButton.setOnClickListener(this);
		}

	private void renderAccountsList() {
		registeredAccountsPanel.removeAllViews();
		accountsPanel.removeAllViews();

		for (String email : accounts.keySet()) {
			AccountService.ACCOUNT_STATUS status = accounts.get(email).status;
			boolean registeredAccount = (status == AccountService.ACCOUNT_STATUS.REGISTERED_IN_PHONE);

			int textColor;

			switch (status) {
			case REGISTERED_IN_PHONE:
				textColor = Color.WHITE;
				break;
			case REGISTERED_IN_SERVER_ONLY:
				textColor = Color.GREEN;
				break;
			default:
				textColor = Color.YELLOW;
				break;
			}

			CheckBox forAccount = new CheckBox(this);
			forAccount.setText(email);
			forAccount.setTextColor(textColor);

			if (registeredAccount) {
				registeredAccountsPanel.setVisibility(View.VISIBLE);
				findViewById(R.id.registeredAccountMessageLabel).setVisibility(
						View.VISIBLE);
				forAccount.setEnabled(false);
				forAccount.setChecked(true);
				registeredAccountsPanel.addView(forAccount);
			} else {
				findViewById(R.id.unregisteredAccMessageLabel).setVisibility(
						View.VISIBLE);
				accountsPanel.addView(forAccount);
			}
		}
	}

		@Override
		public void onClick(View v) {
			List<String> toRegister = new ArrayList<String>();
			for (int i=0; i<accountsPanel.getChildCount(); i++) {
				CheckBox c = (CheckBox) accountsPanel.getChildAt(i);

				if (c.isChecked()) {
					toRegister.add(c.getText().toString());
				}
			}
			new RegisterUserAccounts().execute(toRegister.toArray(new String[toRegister.size()]));
		}

		private class RegisterUserAccounts extends AsyncTask<String, String, Boolean> {
			@Override
			protected Boolean doInBackground(String... params) {
				boolean success = true;
				if (params != null) {
					for (String s : params) {
						onProgressUpdate("Registering account " + s);
						try {
							//AccountService.registerAcount(s, UserRegistrationActivity.this);
						} catch (Exception e) {
							success = false;
						}
					}
				}
				
				return success;
			}

			@Override
			protected void onPostExecute(Boolean success) {
				if (success) {
						hideProgressDialog();
						Runnable r = new Runnable() {
							@Override
							public void run() {
								try {
									accounts = AccountService.getAvailableAccounts(UserRegistrationActivity.this);
								} catch (Exception e) {
									handleError(e);
								}
							}
						};
						Thread t = new Thread(r);
						t.start();
						try {t.join();} catch (Exception ex) {}
						uiHandler.post(updateUi);
				}else {
					hideProgressDialog();
					handleError(new Exception("One or more accounts could not be registered."));
				}
			}

			@Override
			protected void onProgressUpdate(String... values) {
				hideProgressDialog();
				showProgressDialog(values[0]);
			}
		}

		void handleError(Throwable t) {
			//TODO: Handle the errors.
			new AlertDialog.Builder(this).setMessage(t.getMessage()).show();
			Log.e(TAG, t.getMessage());
		}

		protected void showProgressDialog(final String msg) {
			uiHandler.post(new Runnable() {
				@Override
				public void run() {
					progress = ProgressDialog.show(
							UserRegistrationActivity.this, "Please wait...", msg);
				}
			});
		}

		protected void hideProgressDialog() {
			uiHandler.post(new Runnable() {
				@Override
				public void run() {
					if (progress != null) {
						progress.hide();
					}
					progress = null;
				}
			});
		}
}