package com.lahiru.xmes;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;

import com.google.android.gcm.GCMBaseIntentService;
import com.lahiru.xmes.data.CallRequest;
import com.lahiru.xmes.service.DeskPhoneAccountService;
import com.lahiru.xmes.service.DeviceManager;
import com.lahiru.xmes.service.Notifier;
import com.lahiru.xmes.service.TalkToServer;
import com.lahiru.xmes.service.TelephonyService;

/**
 * Recieves messages from the cloud.
 * @author lahiruw
 *
 */
public class GCMIntentService extends GCMBaseIntentService {

	private static List<AccountStatusChangeListener> accountStatusChangeListeners;
	static {
		accountStatusChangeListeners = new ArrayList<GCMIntentService.AccountStatusChangeListener>();
	}

	@Override
	protected void onError(Context context, String arg1) {
		// TODO Handle Error.

	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		try {
			String msgType = intent.getStringExtra("type");

			// Activation status
			if (msgType.equals("activationStatus")) {
				String email = intent.getStringExtra("email");
				boolean authenticated = Boolean.parseBoolean(intent.getStringExtra("isActive"));
				DeskPhoneAccountService.storeAccount(
						context,
						email,
						authenticated);
	
				for (AccountStatusChangeListener listener: accountStatusChangeListeners) {
					listener.onAccountStatusChanged(email, authenticated);
				}
				return;
			}

			// Admin message
			if (msgType.equals("adminmessage")) {
				try {
					String reaction = intent.getStringExtra("reaction");
					String reactionParam1 = intent.getStringExtra("reaction_param1");
					String reactionParam2 = intent.getStringExtra("reaction_param2");
					String message = intent.getStringExtra("message");

					Intent reactionIntent = new Intent(context, Home.class);
					if ("send_email".equals(reaction)) {
						List<String> accounts = DeskPhoneAccountService.getActiveAccounts(context);

						reactionIntent = new Intent(Intent.ACTION_SEND);
						reactionIntent.setType("text/plain");
						if (accounts == null || accounts.isEmpty()) {
							reactionIntent.putExtra(Intent.EXTRA_EMAIL, accounts.get(0));
						}
						reactionIntent.putExtra(Intent.EXTRA_SUBJECT, reactionParam1);
						reactionIntent.putExtra(Intent.EXTRA_TEXT, reactionParam2);
					} else if ("url".equals(reaction)) {
						Uri uri = Uri.parse(reactionParam1);
						reactionIntent = new Intent (Intent.ACTION_VIEW, uri);
					}

					Notifier.createNotification(
							context, "DeskPhone", message, reactionIntent, Notifier.ANNOUNCEMENT);
				} catch (Exception e) {
					ErrorHandler.handleNonUiError(e);
				}

				return;
			}

			// Ping. Reply back with the version number.
			if (msgType.equals("ping")) {
				try {
					JSONObject requestParams = new JSONObject();
					DeviceManager dm = DeviceManager.forContext(context);
					PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
							context.getPackageName(), 0);
					requestParams.put("phoneId", dm.getRegistrationId());
					requestParams.put("dpVersion", packageInfo.versionCode);
					requestParams.put("email", intent.getStringExtra("email"));
					TalkToServer.acknowledgePingRequest(requestParams.toString());
				} catch (Exception e) {
					ErrorHandler.handleNonUiError(e);
				}
				return;
			}
			try {
				if (!DeskPhoneAccountService.isActivatedLocally(context)) {
					return;
				}
			} catch (Exception e) {
				ErrorHandler.handleNonUiError(e);
				return;
			}

			if (msgType.equals("callrequest")) {
				CallRequest callRequest = new CallRequest(
						intent.getStringExtra("number"),
						intent.getStringExtra("note"),
						Long.parseLong(intent.getStringExtra("time")));
				TelephonyService.publishPhoneCallRequest(context, callRequest);
				
			} else if (msgType.equals("sendsms")) {
				TelephonyService.sendSms(
						context, intent.getStringExtra("no"), intent.getStringExtra("message"));
			} 
			else {
				// Not supported feature.
				Uri uri = Uri.parse("market://details?id=com.lahiru.xmes");
				Notifier.createNotification(context,
						"Update DeskPhone", 
						"The feature you are trying to use is not supported in this version. Please update DeskPhone from Google Play",
						new Intent (Intent.ACTION_VIEW, uri) , Notifier.ANNOUNCEMENT);
			}
		} catch (Exception e) {
			ErrorHandler.handleNonUiError(e);
		}
	}

	@Override
	protected void onRegistered(Context context, String regId) {
		DeviceManager dm = DeviceManager.forContext(context);
		try {
			dm.setC2dmHandler(regId);
		} catch (Exception e) {
			ErrorHandler.handleNonUiError(e);
		}
	}

	@Override
	protected void onUnregistered(Context arg0, String arg1) {
		// TODO Inform the server about unregistered device.
	}

	@Override
	protected String[] getSenderIds(Context context) {
		return new String[] {context.getString(R.string.gcm_sender_id)};
	}

	/* LISTENERS */
	public static interface AccountStatusChangeListener {
		void onAccountStatusChanged(String email, boolean isActive);
	}

	public static void addAccountStatusChangeListener(AccountStatusChangeListener listener) {
		accountStatusChangeListeners.add(listener);
	}

	public static void removeAccountStatusChangeListener(AccountStatusChangeListener listener) {
		accountStatusChangeListeners.remove(listener);
	}
}
