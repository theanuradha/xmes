package com.lahiru.xmes;

import com.lahiru.xmes.service.ContactsHelper;
import com.lahiru.xmes.service.DeskPhoneAccountService;
import com.lahiru.xmes.service.DeviceManager;
import com.lahiru.xmes.service.TelephonyService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnPhoneBootReciever extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, Intent intent) {
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {  
      // First, check the phone is an activated phone.
			try {
				boolean activated = DeskPhoneAccountService.isActivatedLocally(context);
				if (!activated) {
					return;
				}
				DeviceManager.forContext(context).checkUpdateStatus();
				TelephonyService.reScheduleAllCalls(context);
				ContactsHelper.startSync(context);
			} catch (Exception e) {
				ErrorHandler.handleNonUiError(e);
			}
		} 
	}
}
